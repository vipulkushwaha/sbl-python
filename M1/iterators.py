# iterators.py

# Iterators are implemented in for loops, comprehensions, generators.
# Iterator is an object that can be iterated upon.

# Iterator objects implement 
# __iter__
# __next__

# Collectively called iterator protocol

my_list = [10, 25, 33, 90]

my_iter = iter(my_list)

print(next(my_iter))
print(next(my_iter))
print(next(my_iter))
print(next(my_iter))

# Another next() will result in exception
# print(next(my_iter))


# Custom iterator
class MyIter:
    def __init__(self, max=0):
        self.max = max
    
    def __iter__(self):
        self.n = 0
        return self
    
    def __next__(self):
        if self.n <= self.max:
            result = 2 ** self.n
            self.n += 1
            return result
        else:
            raise StopIteration

for i in MyIter(5):
    print(i)