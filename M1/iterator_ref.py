# Implemented in for loops, comprehensions, generators etc.
# Iterator in python is an object that can be iterated upon.

# Iterator objects implement:
# __iter__
# __next__
# Collectively called the 'iterator protocol'
# An object that has an iterator is called an iterable

# Example

my_list = [10, 25, 33, 90]

my_iter = iter(my_list)

print(next(my_iter))
print(next(my_iter))
print(next(my_iter))
print(next(my_iter))
# print(next(my_iter))

# How for loop is implemented in Python
"""
iter_obj = iter(iterable)

while True:
    try:
        # Get next item
        element = next(iter_obj)
        # Do something with element
    except StopIteration:
        # if StopIteration is raised, break the loop
        break
"""

# Creating a custom iterator
class MyIter:
    def __init__(self, max=0):
        self.max = max
    def __iter__(self):
        self.n = 0
        return self
    
    def __next__(self):
        if self.n <= self.max:
            result = 2 ** self.n
            self.n += 1
            return result
        else:
            raise StopIteration

for i in MyIter(5):
    print(i)