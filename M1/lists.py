fruits = [
    'apple',
    'mango',
    'cherry',
    'banana',
    'guava',
    [
        'red',
        'green',
        'blue'
    ],
    'watermelon'
    ]

# fruits.append('guava')
# fruits.insert(2, 'peach')

for fruit in fruits:
    print(fruit)