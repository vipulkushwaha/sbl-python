# def say_hello(name, teacher):
#     """
#     This function is used to greet as well as indentify teachers.
#     """

#     print(f"Hello, {name}.")

#     if teacher:
#         print(f'{name} is a teacher.')
#     else:
#         print(f'{name} is a student.')

# say_hello('Vipul', False)



# Prints hello world
# print('Hello World')

# def say_hello(name, teacher):
#     """
#     Is used for greeting.
#     """
#     if teacher:
#         print(f'Hello {name}, you are a teacher.')
    
#     else:
#         print(f'Hello {name}, you are not a teacher.')

# say_hello('Vipul', True)


num = int(input('Enter a number: '))

if num > 0:
    print(f"{num} is a Positive number")
elif num == 0:
    print("You entered zero")
else:
    print(f"{num} is a Negative number")


# # Loops

# # While loop

# count = 0
# while (count < 9):
#    print('The count is:', count)
#    count = count + 1

# print('Good bye!')

# # Break
# i = 1
# while i < 6:
#   print(i)
#   if i == 3:
#     break
#   i += 1

# # Continue
# i = 0
# while i < 6:
#   i += 1
#   if i == 3:
#     continue
#   print(i)

# # While with else
# i = 1
# while i < 6:
#   print(i)
#   i += 1
# else:
#   print("i is no longer less than 6")

# # For loop
# fruits = ["apple", "banana", "cherry"]
# for x in fruits:
#   print(x)

# # Break
# fruits = ["apple", "banana", "cherry"]
# for x in fruits:
#   if x == "banana":
#     break
#   print(x)

# # Continue
# fruits = ["apple", "banana", "cherry"]
# for x in fruits:
#   if x == "banana":
#     continue
#   print(x)

# # Range
# for x in range(6):
#   print(x)

# # Range between
# for x in range(2, 6):
#   print(x)

# # Increment the sequence with 3 (default is 1)
# for x in range(2, 30, 3):
#   print(x)

# # for with else
# for x in range(6):
#   print(x)
# else:
#   print("Finally finished!")

# # If the loop breaks, the else block is not executed
# for x in range(6):
#   if x == 3: break
#   print(x)
# else:
#   print("Finally finished!")

# adj = ["red", "big", "tasty"]
# fruits = ["apple", "banana", "cherry"]

# # Nested for
# for x in adj:
#   for y in fruits:
#     print(x, y)

# # having an empty for loop like this, would raise an error without the pass statement
# for x in [0, 1, 2]:
#   pass
