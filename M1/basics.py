# Input example
print('Input Example')

num = int(input('Enter a number: '))
# print(f'You entered {num}, it is of type {type(num)}.')

# if-elif-else example
if num > 0:
    print(f'{num} is positive.')

elif num == 0:
    print(f'You entered 0.')

else:
    print(f'{num} is negative.')




# While loop
count = 0

while (count < 10):
    print(f'The count is {count}')
    count = count + 1

print('While loop terminated.')



# While loop with break
count = 0

while (count < 10):

    print(f'The count is {count}')
    count = count + 1

    if count == 4:
        break

print('While loop terminated.')


# Continue
count = 0

while (count < 10):
    
    print(f'The count is {count}')
    count = count + 1

    if count == 3:
        print('Condition satisfied.')
        continue

print('While loop terminated.')

# Pass example
if count == 0:
    pass