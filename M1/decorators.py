# decorators.py

# Decorators change and modify functions or methods without directly
# changing any code.

# def f1():
#     print('f1 is called.')

# print(f1)

# # Pass a function as an object to another function
# def f2(f):
#     f()

# f2(f1)


# Decorator definition
def f1(func):

    def wrapper():
        print('Started')
        func()
        print('Ended')
    
    return wrapper


# Target

@f1
def greet():
    print('Hello world.')

greet()