# generators.py

# Iterator implementation requires:
# 
# __iter__ & __next__
# track internal state
# raise StopIteration exception

# Generators to automate this process
# They provide a simple way to create iterators

# Generator is a function that returns an object which can
# iterate over one value at a time.

def my_gen():
    n = 1
    print('This is printed first.')
    yield n

    n += 1
    print('This is printed second.')
    yield n

    n += 1
    print('This is printed last.')
    yield n

# print(my_gen())

for i in my_gen():
    print(i)