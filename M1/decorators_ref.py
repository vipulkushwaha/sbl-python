# # Decorators change and modify functions or methods without
# # directly changing any code

# # Functions can be represented as objects
# def f1():
#     print('Called f1 function')

# # Function is an object
# print(f1)

# # Pass a function as an object to another function
# def f2(f):
#     f()

# f2(f1)

# Decorator definition
def f1(func):
    def wrapper():
        print('Started')
        func()
        print('Ended')

    return wrapper

# Target function
# def greet():
#     print('Hello')

# x = f1(greet)

# x()

@f1
def greet():
    print('Hello')

greet()